       IDENTIFICATION DIVISION. 
       PROGRAM-ID. READFILE.
       AUTHOR. FHANGLERR.

       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL.
           SELECT 100-INPUT-FILE ASSIGN TO "TICKET.DAT"
              ORGANIZATION IS SEQUENTIAL
               FILE STATUS IS WS-INPUT-FILE-STATUS.

       DATA DIVISION. 
       FILE SECTION. 
       FD  100-INPUT-FILE
           BLOCK CONTAINS 0 RECORDS.
       01 100-INPUT-RECORD.
          05 DATES              PIC X(12).
          05 TIMESS             PIC X(7).      
          05 SEX                PIC X(1).
          05 COMMAS             PIC X(2).
          05 AGE                PIC X(3).
          05 FILLER             PIC x(55).

       WORKING-STORAGE SECTION.
       01 WS-CALCULATION.
          05 WS-INPUT-COUNT     PIC 9(8).
          05 WS-COUNT-M         PIC 9(4).
          05 WS-COUNT-F         PIC 9(4).
          05 WS-TICKET.
             10 WS-DATES        PIC X(10).
             10 WS-TIME         PIC X(5).
             10 WS-SEX          PIC X(1).
             10 WS-AGE          PIC X(3).
       01 WS-INPUT-FILE-STATUS  PIC   X(2).
          88 FILE-SUCCESS                  VALUE '00'.
          88 FILE-AT-END                   VALUE '10'.

       PROCEDURE DIVISION.
           OPEN INPUT 100-INPUT-FILE
           
           READ 100-INPUT-FILE 
           PERFORM UNTIL FILE-AT-END 
      *            DISPLAY 100-INPUT-RECORD
                   READ 100-INPUT-FILE
                   IF FILE-SUCCESS OF WS-INPUT-FILE-STATUS 
      *               DISPLAY WS-COUNT-M             
                      EVALUATE SEX 
                          WHEN 'M'
      *                      DISPLAY SEX
                             ADD +1 TO WS-COUNT-M
                          WHEN 'F'
      *                      DISPLAY SEX
                             ADD +1 TO WS-COUNT-F
                      END-EVALUATE
                   ELSE
                   DISPLAY "M:" WS-COUNT-M
                   DISPLAY "F:" WS-COUNT-F
           END-PERFORM

           CLOSE 100-INPUT-FILE
           GOBACK.